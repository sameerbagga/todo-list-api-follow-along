'use strict';

const express = require('express');

// const {Router} = require('express');
// router = Router();

// Create instance of router
const router = express.Router();

const db = require ('./db');


router.get('/item', (req, res) => {
    res.json(db);
});

router.get('/item/:id', (req, res) => {
    const id = parseInt(req.params.id, 10);
    let matchedItem;
    db.forEach((item) => {
        if (item.id ===id) {
            matchedItem = item;
        }
    });

    //Send matched item if it exists
    if (matchedItem) {
        res.json(matchedItem);        
    } else { // Does not exist
        res
            .status(404)   //Send 404 status response (Not Found)
            .end();       //Send / finish the request
    }
    
});

// Export router
module.exports = router;