'use strict';

const express = require('express');

const router = require('./router');

const app = express();

// Apply router as middleware
app.use(router);


// Server Starts
app.listen(3000, () => {
    console.log('Server is Running');
});